import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import {ThaiAgePipe} from '../pipes/to-thai-age.pipe';
import {ThaiDatePipe} from '../pipes/to-thai-date-pipe';



@NgModule({
  declarations: [ ThaiAgePipe,ThaiDatePipe],
  imports: [
    CommonModule,
  ],
  // exports is required so you can access your component/pipe in other modules
  exports: [ThaiAgePipe,ThaiDatePipe]
})
export class SharedModule{}