import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChangeWardRoutingModule } from './change-ward-routing.module';
import { ChangeWardComponent } from './change-ward.component';


@NgModule({
  declarations: [
    ChangeWardComponent
  ],
  imports: [
    CommonModule,
    ChangeWardRoutingModule
  ]
})
export class ChangeWardModule { }
